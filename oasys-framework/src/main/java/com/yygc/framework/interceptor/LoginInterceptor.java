package com.yygc.framework.interceptor;

import com.yygc.common.utils.JWTUtil;
import com.yygc.common.utils.RSAUtil;
import com.yygc.common.utils.StringUtil;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.PublicKey;

public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String url = request.getRequestURI();
        String token = "";
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("token")) {
                token = cookie.getValue();
            }
        }
        if (StringUtil.isEmpty(token)) {
            if (url.indexOf("/login") >= 0) {
                return true;
            } else {
                //返回401状态码，应由前端根据状态码跳转到相应的页面（登录页）
//                response.sendError(response.SC_UNAUTHORIZED);
                request.getRequestDispatcher("/login").forward(request, response);
                return false;//响应401
            }
        } else {
//            System.out.println(token);
            PublicKey publicKey = null;
            try {
                publicKey = RSAUtil.getPublicKey("D:/public.rsa");
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                JWTUtil.getInfoFromToken(token, publicKey);
                //放行
                return true;
            } catch (Exception e) {
                //401，逻辑同上
                request.getRequestDispatcher("/login").forward(request, response);
                return false;
            }
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }
}
