package com.yygc.common.exception;

/**
 * 文件信息异常类
 */
public class FileException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    /**
     * 错误码
     */
    private String code;

    /**
     * 错误码对应的参数
     */
    private Object[] args;

    public FileException(String code, Object[] args) {
        this.code = code;
        this.args = args;
    }
}
