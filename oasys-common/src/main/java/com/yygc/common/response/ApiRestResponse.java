package com.yygc.common.response;

/**
 * 数据统一响应格式
 * status：响应状态码
 * message：响应信息
 * data：响应数据
 */
public class ApiRestResponse<T> {
    private Integer status;
    private String message;
    private T data;

    /**
     * 空构造方法，避免反序列化问题
     */
    public ApiRestResponse() {
    }

    public ApiRestResponse(Integer status, String message, T data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
