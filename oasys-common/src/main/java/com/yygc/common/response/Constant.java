package com.yygc.common.response;

/**
 * 定义一些系统需要的常量
 */
public class Constant {
    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";
    /**
     * 10000 登录失败
     */
    public static final int STATUS_LOGIN_ERROR = 10000;
    /**
     * 10001 登陆成功
     */
    public static final int STATUS_LOGIN_SUCCESS = 10001;
    /**
     * 10002 注册失败
     */
    public static final int STATUS_REGISTER_ERROR = 10002;
    /**
     * 10003 注册成功
     */
    public static final int STATUS_REGISTER_SUCCESS = 10003;
    /**
     * 10004 录入失败
     */
    public static final int STATUS_PUBLIC_TASKS_ERROR = 10004;
    /**
     * 10005 录入成功
     */
    public static final int STATUS_PUBLIC_TASKS_SUCCESS = 10005;
    /**
     * 10006 删除失败
     */
    public static final int STATUS_DELETE_ERROR = 10006;
    /**
     * 10007 删除成功
     */
    public static final int STATUS_DELETE_SUCCESS = 10007;
    /**
     * 10008 发送失败
     */
    public static final int STATUS_SEND_ERROR = 10008;
    /**
     * 10009 发送成功
     */
    public static final int STATUS_SEND_SUCCESS = 10009;
    /**
     * 10010 上传失败
     */
    public static final int STATUS_UPLOAD_ERROR = 10010;
    /**
     * 10011 上传成功
     */
    public static final int STATUS_UPLOAD_SUCCESS = 10011;
}
