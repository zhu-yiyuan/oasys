package com.yygc.common.utils;

public class StringUtil {
    /** 空字符串 */
    private static final String NULLSTR = "";

    public static boolean isEmpty(String param) {
        return param == null || "".equals(param.trim());
    }

    /**
     * 截取字符串
     *
     * @param str 字符串
     * @param start 开始
     * @return 结果
     */
    public static String substring(final String str, int start)
    {
        if (str == null)
        {
            return NULLSTR;
        }

        if (start < 0)
        {
            start = str.length() + start;
        }

        if (start < 0)
        {
            start = 0;
        }
        if (start > str.length())
        {
            return NULLSTR;
        }

        return str.substring(start);
    }
}
