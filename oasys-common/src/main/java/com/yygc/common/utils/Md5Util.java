package com.yygc.common.utils;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * 密码加密
 */
public class Md5Util {
    public static String md5Digest(String source) {
        return DigestUtils.md5Hex(source);
    }

    public static String md5Digest(String source, Integer salt) {
        char[] c = source.toCharArray();
        for (int i = 0; i < c.length; i++) {
            c[i] = (char) (c[i] + salt);
        }
        String newSource = new String(c);//产生一个新的字符串
        return md5Digest(newSource);
    }
}
