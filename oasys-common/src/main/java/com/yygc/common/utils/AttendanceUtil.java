package com.yygc.common.utils;

/**
 * 考勤工具类
 * @author Masker
 */
public class AttendanceUtil {
    /** 经纬度转换常量 */
    private static final double EARTH_RADIUS = 6378137.0;

    /**
     * 计算AB两点距离(米)
     * @param lat_a    A点纬度
     * @param lng_a    A点经度
     * @param lat_b    B点纬度
     * @param lng_b    B点经度
     * @return    AB两点距离多少米
     */
    public static Double getDistance(double lat_a, double lng_a, double lat_b, double lng_b) {
        double radLat1 = (lat_a * Math.PI / 180.0);
        double radLat2 = (lat_b * Math.PI / 180.0);
        double a = radLat1 - radLat2;
        double b = (lng_a - lng_b) * Math.PI / 180.0;
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)
                + Math.cos(radLat1) * Math.cos(radLat2)
                * Math.pow(Math.sin(b / 2), 2)));
        s = s * EARTH_RADIUS;
        s = Math.round(s * 10000) / 10000;
        return s;
    }

    /**
     * 将用“,”分隔的经纬度字符串解析为Double型的经纬度
     * @param lat_lon
     * @return 0 纬度 1 经度
     */
    public static Double[] toNumber(String lat_lon){
        Double[] latAndLon = new Double[2];
        int flag = lat_lon.indexOf(",");
        latAndLon[0] = Double.valueOf(lat_lon.substring(0,flag));
        latAndLon[1] = Double.valueOf(lat_lon.substring(flag+1));

        return latAndLon;
    }

    /**
     * 获取小时
     * @param time
     * @return
     */
    public static int getHour(String time){
        return Integer.parseInt(time.substring(0,time.indexOf(":")));
    }

    /**
     * 获取分钟
     * @param time
     * @return
     */
    public static int getMinute(String time){
        return Integer.parseInt(time.substring(time.indexOf(":")+1));
    }

    /**
     * 时间格式符合 “hh:mm"
     * @param time
     * @return
     */
    public static boolean isCorrectFormat(String time){
        String temp = time.trim();
        int markInd = temp.indexOf(":");

        if(markInd == 2){
            try{
                String front = temp.substring(0,markInd);
                Integer.parseInt(front);
                String latter = temp.substring(markInd+1);
                Integer.parseInt(latter);
                return true;
            }catch (NumberFormatException e){
                return false;
            }
        }

        return false;
    }
}
