package com.yygc.web.pojo;

import lombok.Data;

@Data
public class FormalStaff {
    private Integer sta_id;
    private String sta_title;
    private Integer sta_prop_id;
    private Integer sta_ver_id;

    /**
     * 实习/试用心得
     */
    private String sta_exp;

    /**
     * 本岗位职责理解
     */
    private String sta_und;

    /**
     * 对公司的建议及理解
     */
    private String sta_sug;

    public FormalStaff(String title, Integer prop_id, Integer ver_id,
                       String experience, String understand, String suggest) {
        this.sta_title = title;
        this.sta_prop_id = prop_id;
        this.sta_ver_id = ver_id;
        this.sta_exp = experience;
        this.sta_und = understand;
        this.sta_sug = suggest;
    }
}
