package com.yygc.web.pojo;

import lombok.Data;

/**
 * 报销科目
 */
@Data
public class Subject {
    private Integer sub_id;
    private String sub_name;

    public Subject(Integer sub_id, String sub_name) {
        this.sub_id = sub_id;
        this.sub_name = sub_name;
    }
}
