package com.yygc.web.pojo;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 流程申请实体类
 *
 * @author Masker
 */
@Data
public class Process {
    private Integer pro_id;
    private String pro_type_name;
    @Value("0")
    private Integer pro_checked_status;
    private String pro_title;
    private String pro_proposer;
    private String pro_verifier;
    private String pro_descri;

    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date pro_create_time;

    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date pro_end_time;

    private String pro_attach;

    public Process(Integer pro_id, String pro_type_name, String pro_title, String pro_proposer,
                   String pro_verifier, String pro_descri, Date pro_create_time) {
        this.pro_id = pro_id;
        this.pro_type_name = pro_type_name;
        this.pro_title = pro_title;
        this.pro_proposer = pro_proposer;
        this.pro_verifier = pro_verifier;
        this.pro_descri = pro_descri;
        this.pro_create_time = pro_create_time;
    }

    public Process(String pro_type_name, String pro_title, String pro_proposer,
                   String pro_verifier, String pro_descri, Date pro_create_time) {
        this.pro_type_name = pro_type_name;
        this.pro_title = pro_title;
        this.pro_proposer = pro_proposer;
        this.pro_verifier = pro_verifier;
        this.pro_descri = pro_descri;
        this.pro_create_time = pro_create_time;
    }
}
