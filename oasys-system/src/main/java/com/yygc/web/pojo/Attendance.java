package com.yygc.web.pojo;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 考勤实体
 */
@Data
public class Attendance {
    private Integer id;
    private Integer user_id;
    private String site;
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date create_time;
    /**
     * 准时 0 迟到 1 早退 2 未在指定范围内 -1
     */
    private Integer status;
    /**
     * 0 正常  1 删除状态
     */
    @Value("0")
    private Integer existence;

    public Attendance(Integer user_id) {
        this.user_id = user_id;
    }

    public Attendance(Integer id, Integer user_id, String site, Date date) {
        this.id = id;
        this.user_id = user_id;
        this.site = site;
        this.create_time = date;
    }

    public Attendance(Integer user_id, String site, Date date) {
        this.user_id = user_id;
        this.site = site;
        this.create_time = date;
    }
}
