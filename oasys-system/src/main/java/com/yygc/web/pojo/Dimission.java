package com.yygc.web.pojo;

import lombok.Data;

@Data
public class Dimission {
    /**
     * 离职申请流程 id
     */
    private Integer dim_id;

    /**
     * 标题
     */
    private String dim_title;

    /**
     * 申请人 id
     */
    private Integer dim_pro_id;

    /**
     * 交接人 id
     */
    private Integer dim_hand_id;

    /**
     * 未完成事宜
     */
    private String dim_thing;

    /**
     * 离职原因
     */
    private String dim_rea;

    /**
     * 审核人 id
     */
    private Integer dim_ver_id;

    public Dimission(String dim_title, Integer dim_pro_id, Integer dim_hand_id,
                     String dim_thing, String dim_rea, Integer dim_ver_id) {
        this.dim_title = dim_title;
        this.dim_pro_id = dim_pro_id;
        this.dim_hand_id = dim_hand_id;
        this.dim_thing = dim_thing;
        this.dim_rea = dim_rea;
        this.dim_ver_id = dim_ver_id;
    }
}
