package com.yygc.web.pojo;

import lombok.Data;

@Data
public class LeaveType {
    private Integer lea_id;
    private String lea_name;

    public LeaveType(Integer lea_id, String lea_name) {
        this.lea_id = lea_id;
        this.lea_name = lea_name;
    }
}
