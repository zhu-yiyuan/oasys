package com.yygc.web.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class Leave {
    /**
     * 请假流程 id
     */
    private Integer lea_id;

    /**
     * 请假流程 标题
     */
    private String lea_title;

    /**
     * 请假流程 申请人 id
     */
    private Integer lea_prop_id;

    /**
     * 请假类型 id
     */
    private Integer lea_type_id;

    /**
     * 请假开始日期
     */
    private Date lea_begin_date;

    /**
     * 请假结束日期
     */
    private Date lea_end_date;

    /**
     * 请假天数
     */
    private Integer lea_days;

    /**
     * 审核人 id
     */
    private Integer lea_ver_id;

    /**
     * 请假事由
     */
    private String lea_rea;

    public Leave(String lea_title, Integer lea_prop_id, Integer lea_type_id, Date lea_begin_date,
                 Date lea_end_date, Integer lea_days, Integer lea_ver_id, String lea_rea) {
        this.lea_title = lea_title;
        this.lea_prop_id = lea_prop_id;
        this.lea_type_id = lea_type_id;
        this.lea_begin_date = lea_begin_date;
        this.lea_end_date = lea_end_date;
        this.lea_days = lea_days;
        this.lea_ver_id = lea_ver_id;
        this.lea_rea = lea_rea;
    }
}
