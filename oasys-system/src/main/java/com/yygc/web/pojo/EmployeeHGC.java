package com.yygc.web.pojo;

import lombok.Data;

@Data
public class EmployeeHGC {
    private Integer emp_id;
    private String emp_name;
    private String emp_name_spell;
}
