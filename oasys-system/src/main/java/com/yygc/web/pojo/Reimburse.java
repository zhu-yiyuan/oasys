package com.yygc.web.pojo;

import lombok.Data;

//报销方式
@Data
public class Reimburse {
    private Integer rei_id;
    private String rei_type;

}
