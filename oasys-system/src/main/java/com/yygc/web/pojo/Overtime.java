package com.yygc.web.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class Overtime {
    private Integer ov_id;
    private String ov_title;
    /**
     * 加班类型id
     */
    private Integer ov_type_id;
    private Date ov_begin_time;
    private Date ov_end_time;
    /**
     * 加班事由
     */
    private String ov_rea;
    /**
     * 审核人id
     */
    private Integer ov_ver_id;

    public Overtime(String ov_title, Integer ov_type_id, Date ov_begin_time,
                    Date ov_end_time, String ov_rea, Integer ov_ver_id) {
        this.ov_title = ov_title;
        this.ov_type_id = ov_type_id;
        this.ov_begin_time = ov_begin_time;
        this.ov_end_time = ov_end_time;
        this.ov_rea = ov_rea;
        this.ov_ver_id = ov_ver_id;
    }
}
