package com.yygc.web.pojo;

import lombok.Data;

@Data
public class OverType {
    private Integer ovty_id;
    private String ovty_name;

    public OverType(Integer ovty_id, String ovty_name) {
        this.ovty_id = ovty_id;
        this.ovty_name = ovty_name;
    }
}
