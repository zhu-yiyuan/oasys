package com.yygc.web.pojo;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 费用报销
 */
@Data
public class Expense {
    private Integer exp_id;
    private String exp_title;
    private Integer exp_prop_id;
    private Integer exp_rei_type_id;
    private Integer exp_ver_id;
    /**
     * 报销事由
     */
    private String exp_rea;
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date exp_create_time;
    /**
     * 报销科目
     */
    private Integer exp_sub_id;
    private Double exp_count;
    //缺少报销凭证

    public Expense(String title, Integer propId, Integer reiTypeId, Integer verId,
                   String reason, Date createTime, Integer subId, Double count) {
        this.exp_title = title;
        this.exp_prop_id = propId;
        this.exp_rei_type_id = reiTypeId;
        this.exp_ver_id = verId;
        this.exp_rea = reason;
        this.exp_create_time = createTime;
        this.exp_sub_id = subId;
        this.exp_count = count;
    }
}
