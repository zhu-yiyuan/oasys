package com.yygc.web.service;

import com.yygc.web.mapper.LeaveTypeMapper;
import com.yygc.web.pojo.LeaveType;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class LeaveTypeService {
    @Resource
    LeaveTypeMapper leaveTypeMapper;

    public LeaveType findOneByName(String typeName) {
        return leaveTypeMapper.findLeaveTypeByName(typeName);
    }
}
