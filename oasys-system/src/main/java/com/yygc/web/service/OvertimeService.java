package com.yygc.web.service;

import com.yygc.web.mapper.OvertimeMapper;
import com.yygc.web.pojo.Overtime;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class OvertimeService {
    @Resource
    OvertimeMapper overtimeMapper;

    public int addOne(Overtime overtime) {
        return overtimeMapper.addOne(overtime);
    }
}
