package com.yygc.web.service;

import com.yygc.web.mapper.DimissionMapper;
import com.yygc.web.pojo.Dimission;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class DimissionService {
    @Resource
    DimissionMapper dimissionMapper;

    public int addOneDimission(Dimission dimission) {
        return dimissionMapper.addOneDimission(dimission);
    }
}
