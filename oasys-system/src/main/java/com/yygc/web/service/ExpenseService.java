package com.yygc.web.service;

import com.yygc.web.mapper.ExpenseMapper;
import com.yygc.web.pojo.Expense;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ExpenseService {
    @Resource
    ExpenseMapper expenseMapper;

    public int addOneExpense(Expense expense) {
        return expenseMapper.addOne(expense);
    }
}
