package com.yygc.web.service;

import com.yygc.web.mapper.FormalStaffMapper;
import com.yygc.web.pojo.FormalStaff;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class FormalStaffService {
    @Resource
    FormalStaffMapper formalStaffMapper;

    public int addOneFormalStaff(FormalStaff formalStaff) {
        return formalStaffMapper.addOne(formalStaff);
    }
}
