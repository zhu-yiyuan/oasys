package com.yygc.web.service;

import com.yygc.web.mapper.EmployeeMapper;
import com.yygc.web.pojo.EmployeeHGC;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class EmployeeService {
    @Resource
    EmployeeMapper employeeMapper;

    public EmployeeHGC findEmpByName(String name) {
        return employeeMapper.findEmpByName(name);
    }
}
