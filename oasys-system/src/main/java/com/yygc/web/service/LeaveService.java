package com.yygc.web.service;

import com.yygc.web.mapper.LeaveMapper;
import com.yygc.web.pojo.Leave;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class LeaveService {
    @Resource
    LeaveMapper leaveMapper;

    public int addOneLeave(Leave leave) {
        return leaveMapper.addOneLeave(leave);
    }
}
