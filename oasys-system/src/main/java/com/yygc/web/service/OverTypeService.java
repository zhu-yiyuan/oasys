package com.yygc.web.service;

import com.yygc.web.mapper.OverTypeMapper;
import com.yygc.web.pojo.OverType;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class OverTypeService {
    @Resource
    OverTypeMapper overTypeMapper;

    public OverType findOneById(int ovty_id) {
        return overTypeMapper.findOneById(ovty_id);
    }

    public OverType findOneByName(String typeName) {
        return overTypeMapper.findOneByName(typeName);
    }
}
