package com.yygc.web.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yygc.common.exception.WebException;
import com.yygc.common.utils.AttendanceUtil;
import com.yygc.web.mapper.AttendanceMapper;
import com.yygc.web.pojo.Attendance;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class AttendanceService {

    /**
     * 公司的地址
     */
    private static String COMPANY_LOCATION = "广东省广州市黄埔区凝彩路26号";

    /**
     * 公司的地理纬度
     */
    private static Double COMPANY_LATITUDE = 23.171078;

    /**
     * 公司的地理经度
     */
    private static Double COMPANY_LONGITUDE = 113.451843;

    /**
     * 允许的距离差
     */
    private static final Double DISTANCE_ALLOW = 20.0;

    /**
     * 公司的上班时间
     */
    private static String COMPANY_ON_DUTY = "09:30";

    /**
     * 公司的下班时间
     */
    private static String COMPANY_OFF_DUTY = "18:30";

    /**
     * 打卡间隔时间差
     */
    private static int TIME_DIFFERENCE = 6;

    /**
     * 迟到的考勤状态
     */
    private static final Integer LATE_FOR_WORK = 1;

    /**
     * 早退的考勤状态
     */
    private static final Integer EARLY_TO_LEAVE = 2;

    /**
     * 不在公司范围内的考勤状态
     */
    private static final Integer NOT_IN_COMPANY = -1;

    /**
     * 准时上班的考勤状态
     */
    private static final Integer ON_TIME_FOR_WORK = 0;

    @Resource
    AttendanceMapper attendanceMapper;

    /**
     * 添加一条考勤记录
     *
     * @param attendance
     * @return
     */
    public int addOneAttendance(Attendance attendance) {
        int result = -1;
        if (!ifRepeat(attendance)) {
            this.setAttendanceStatus(attendance);

            result = attendanceMapper.addOneAttendance(attendance);
        }

//        result = attendanceMapper.addOneAttendance(attendance);
        return result;
    }

    /**
     * 查询单条考勤记录信息
     *
     * @param attendance
     * @return
     */
    public Attendance queryAttendance(Attendance attendance) {
        return attendanceMapper.queryAttendance(attendance);
    }

    /**
     * 查询多条考勤纪录
     *
     * @param attendance
     * @return
     */
    public List<Attendance> queryListAttendance(Attendance attendance) {
        return attendanceMapper.queryLastAttenToday(attendance);
    }

    public List<Attendance> queryListAllAttendance() {
        return attendanceMapper.queryAll();
    }

    /**
     * 删除单条考勤记录
     *
     * @param attendance
     * @return
     */
    public int removeOneAttendance(Attendance attendance) {
        return attendanceMapper.deleteOneAttendance(attendance);
    }

    public int removeAttendances(List<Integer> ids) {
        return attendanceMapper.deleteAttendances(ids);
    }

    /**
     * 从远程接口返回的结果中获取经纬度信息
     *
     * @param result
     */
    public void getLatLngFromRes(String result) {
        JSONObject jsonObject = JSON.parseObject(result);
        Integer status = jsonObject.getInteger("status");
        if ("0".equals(status)) {
            JSONObject jsonResult = jsonObject.getJSONObject("result");
            JSONObject location = jsonResult.getJSONObject("location");
            Double lat = location.getDouble("lat");
            setCompanyLatitude(lat);
            Double lng = location.getDouble("lng");
            setCompanyLongitude(lng);
        } else {
            throw new WebException("获取数据失败！");
        }
    }

    /**
     * 设置考勤记录的状态信息
     * 迟到、早退、准时、不在公司指定范围内
     *
     * @param attendance
     */
    public void setAttendanceStatus(Attendance attendance) {
        if (isInTheCompany(attendance.getSite())) {
            if (isLateForWork(attendance.getCreate_time())) {
                attendance.setStatus(LATE_FOR_WORK);
            } else if (isEarlyToLeave(attendance.getCreate_time())) {
                attendance.setStatus(EARLY_TO_LEAVE);
            } else {
                attendance.setStatus(ON_TIME_FOR_WORK);
            }
        } else {
            attendance.setStatus(NOT_IN_COMPANY);
        }
    }

    /**
     * 判断在一段时间内是否重复添加考勤记录
     *
     * @param attendance
     * @return
     */
    public boolean ifRepeat(Attendance attendance) {
        java.sql.Date today = new java.sql.Date(System.currentTimeMillis());
        Attendance queryAttendance = new Attendance(attendance.getUser_id(), null, today);
        List<Attendance> attenList = this.queryListAttendance(queryAttendance);

        if (attenList != null && attenList.size() > 0) {
            Attendance temp = attenList.get(0);
            if (this.gapHours(attendance, temp) > TIME_DIFFERENCE) {
                return false;
            } else {
                return true;
            }
        }

        return false;
    }

    /**
     * 判断是否在公司范围内
     *
     * @param userLocation
     * @return
     */
    public boolean isInTheCompany(String userLocation) {
        Double[] userLatLong = new Double[2];
        userLatLong = AttendanceUtil.toNumber(userLocation);

        Double fromUserToCompany = AttendanceUtil.getDistance(userLatLong[0], userLatLong[1], COMPANY_LATITUDE, COMPANY_LONGITUDE);
        if (fromUserToCompany > DISTANCE_ALLOW) {
            return false;
        }
        return true;
    }

    /**
     * 判断是否迟到
     *
     * @param userOnDuty
     * @return
     */
    public boolean isLateForWork(Date userOnDuty) {
        Calendar timeOfUser = Calendar.getInstance();
        timeOfUser.setTime(userOnDuty);
        int hourOfUserTime = timeOfUser.get(Calendar.HOUR);
        int hourOfCompanyStandard = AttendanceUtil.getHour(COMPANY_ON_DUTY);

        if (hourOfUserTime <= hourOfCompanyStandard) {
            int minuteOfUserTime = timeOfUser.get(Calendar.MINUTE);
            int minuteOfCompanyStandard = AttendanceUtil.getMinute(COMPANY_ON_DUTY);

            if (hourOfUserTime == hourOfCompanyStandard) {
                if (minuteOfUserTime <= minuteOfCompanyStandard) {
                    return false;
                }
                return true;
            }

            return false;
        }

        return true;
    }

    /**
     * 判断是否早退
     *
     * @param userOffDuty
     * @return
     */
    public boolean isEarlyToLeave(Date userOffDuty) {
        Calendar timeOfUser = Calendar.getInstance();
        timeOfUser.setTime(userOffDuty);
        int hourOfUserTime = timeOfUser.get(Calendar.HOUR);
        int hourOfCompanyStandard = AttendanceUtil.getHour(COMPANY_OFF_DUTY);

        if (hourOfUserTime >= hourOfCompanyStandard) {
            int minuteOfUserTime = timeOfUser.get(Calendar.MINUTE);
            int minuteOfCompanyStandard = AttendanceUtil.getMinute(COMPANY_OFF_DUTY);

            if (hourOfUserTime == hourOfCompanyStandard) {
                if (minuteOfUserTime >= minuteOfCompanyStandard) {
                    return false;
                }
                return true;
            }

            return false;
        }

        return true;
    }

    /**
     * 获取距离上次打卡的时间差
     *
     * @param now
     * @param before
     * @return
     */
    public int gapHours(Attendance now, Attendance before) {
        long timeOfNow = now.getCreate_time().getTime();
        long timeOfBefore = before.getCreate_time().getTime();
        int gapHour = (int) ((timeOfNow - timeOfBefore) / (1000 * 60 * 60));

        return gapHour;
    }


    /**
     * 修改公司位置
     *
     * @param comLoc
     */
    public void setCompanyLocation(String comLoc) {
        COMPANY_LOCATION = comLoc;
    }

    /**
     * 修改公司经纬度
     *
     * @param comLat
     * @param comLon
     */
    public void setCompanyLatitudeAndLongitude(Double comLat, Double comLon) {
        COMPANY_LATITUDE = comLat;
        COMPANY_LONGITUDE = comLon;
    }

    public void setCompanyLatitude(Double comLat) {
        COMPANY_LATITUDE = comLat;
    }

    public void setCompanyLongitude(Double comLon) {
        COMPANY_LONGITUDE = comLon;
    }

    /**
     * 修改允许再次打卡的时间差
     *
     * @param time
     */
    public void setTimeDifference(int time) {
        TIME_DIFFERENCE = time;
    }

    /**
     * 修改公司上班时间
     *
     * @param onDuty
     */
    public void setOnDutyTime(String onDuty) {
        if (AttendanceUtil.isCorrectFormat(onDuty)) {
            COMPANY_ON_DUTY = onDuty;
        } else {
            throw new WebException("格式错误！");
        }
    }

    /**
     * 修改公司下班时间
     *
     * @param offDuty
     */
    public void setOffDutyTime(String offDuty) {
        if (AttendanceUtil.isCorrectFormat(offDuty)) {
            COMPANY_OFF_DUTY = offDuty;
        } else {
            throw new WebException("格式错误");
        }
    }
}
