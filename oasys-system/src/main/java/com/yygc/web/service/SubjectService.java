package com.yygc.web.service;

import com.yygc.web.mapper.SubjectMapper;
import com.yygc.web.pojo.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubjectService {
    @Autowired
    SubjectMapper subjectMapper;

    public Subject findOneByName(String name) {
        return subjectMapper.findOneByName(name);
    }
}
