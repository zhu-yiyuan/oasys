package com.yygc.web.service;

import com.yygc.web.mapper.ReimburseMapper;
import com.yygc.web.pojo.Reimburse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReimburseService {
    @Autowired
    ReimburseMapper reimburseMapper;

    public Reimburse findOneByName(String typeName) {
        return reimburseMapper.findOneByTypeName(typeName);
    }
}
