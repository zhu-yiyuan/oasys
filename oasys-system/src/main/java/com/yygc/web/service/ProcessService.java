package com.yygc.web.service;

import com.yygc.web.mapper.EmployeeMapper;
import com.yygc.web.mapper.ProcessMapper;
import com.yygc.web.pojo.Process;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ProcessService {
    @Resource
    ProcessMapper processMapper;

    @Resource
    EmployeeMapper employeeMapper;

    public int addOneProcess(Process process) {
        return processMapper.addOne(process);
    }

    public List<Process> queryListByProposer(String proposer) {
        return processMapper.queryListByProposer(proposer);
    }

    public List<Process> queryListByVerifier(String verifier) {
        return processMapper.queryListByVerifier(verifier);
    }

    public int updateProcessCheckedStatus(int proc_id, int status) {
        return processMapper.updateProcessCheckedStatus(proc_id, status);
    }

    public List<Process> queryAllProcess() {
        return processMapper.queryAllProcess();
    }

    public int deleteProcesses(List<Integer> ids) {
        return processMapper.deleteProcess(ids);
    }
}
