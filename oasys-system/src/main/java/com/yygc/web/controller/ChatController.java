package com.yygc.web.controller;

import com.yygc.domain.vo.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

@Controller
@RequestMapping("/user/common")
public class ChatController {
    @RequestMapping("/chat")
    public ModelAndView chat(HttpServletRequest request, HttpSession session) throws UnknownHostException {
        User user = (User) session.getAttribute("user");
        String name = user.getUserAccount();
        ModelAndView mav = new ModelAndView("chat");
        mav.addObject("username", name);
        mav.addObject("webSocketUrl", "ws://" + InetAddress.getLocalHost().getHostAddress() + ":" + request.getServerPort() + request.getContextPath() + "/chat");
        return mav;
    }
}
