package com.yygc.web.controller.comm;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user/comm")
public class CommController {
    @RequestMapping("/comm-index")
    public String toIndex() {
        return "comm/comm-index";
    }

    @RequestMapping("/index")
    public String index() {
        return "comm/index";
    }

    @RequestMapping("/tasks")
    public String task() {
        return "comm/my-tasks";
    }

    @RequestMapping("/process/bursement")
    public String toBursement() {
        return "comm/process/bursement";
    }

    @RequestMapping("/process/overtime")
    public String toOvertime() {
        return "comm/process/overtime";
    }

    @RequestMapping("/process/regular")
    public String toregular() {
        return "comm/process/regular";
    }

    @RequestMapping("/process/holiday")
    public String toholiday() {
        return "comm/process/holiday";
    }

    @RequestMapping("/process/dimission")
    public String todimission() {
        return "comm/process/dimission";
    }

    @RequestMapping("/attendance")
    public String toattendance() {
        return "comm/attendance";
    }
}
