package com.yygc.web.controller.comm;

import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class FileDownloadController {
    @RequestMapping("/files/filenames")
    public ModelAndView getFilenames(ModelAndView mav) {
        List<String> filenameLists = new ArrayList<>();
        File f = new File("D:\\oasys\\uploadPath");
        File[] listFiles = f.listFiles();
        for (File f1 : listFiles) {
            if (f1.isDirectory()) {
                filenameLists = FileDownloadController.getFilenames(f1, filenameLists);
            }
        }
        mav.addObject("filenameLists", filenameLists);
        mav.setViewName("comm/filedownload");
        return mav;
    }

    @RequestMapping("/file-download/files/{filenames}")
    public ResponseEntity<byte[]> fileDownload(@PathVariable("filenames") String filename) throws UnsupportedEncodingException {
        List<String> filepathLists = new ArrayList<>();
        // 指定要下载的文件根路径
        String dirPath = "D:/oasys/uploadPath/";
        String filePath = "";
        // 创建该文件对象
        File file = new File(dirPath);
        File[] listFiles = file.listFiles();
        for (File f1 : listFiles) {
            if (f1.isDirectory()) {
                filepathLists = FileDownloadController.getFilePath(f1, filepathLists);
            }
        }
        Optional<String> path = filepathLists.stream()
                .filter(filepath -> filepath.substring(filepath.lastIndexOf("\\") + 1).equals(filename))
                .findFirst();
        file = new File(path.get());
        // 设置响应头
        HttpHeaders headers = new HttpHeaders();
        // 通知浏览器以下载方式打开
        headers.setContentDispositionFormData("attachment", URLEncoder.encode(filename, "UTF-8"));
        // 定义以流的形式下载返回文件数据
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        try {
            return new ResponseEntity<>(FileUtils.readFileToByteArray(file), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<byte[]>(e.getMessage().getBytes(), HttpStatus.EXPECTATION_FAILED);
        }
    }

    public static List<String> getFilenames(File f, List<String> filenameLists) {
        File[] listFiles = f.listFiles();
        String filename = "";
        for (File f1 : listFiles) {
            if (f1.isDirectory()) {
                getFilenames(f1, filenameLists);
            } else {
                filename = f1.toString();
                filenameLists.add(filename.substring(filename.lastIndexOf("\\") + 1));
            }
        }
        return filenameLists;
    }

    public static List<String> getFilePath(File f, List<String> filepathLists) {
        File[] listFiles = f.listFiles();
        String filepath = "";
        for (File file : listFiles) {
            if (file.isDirectory()) {
                getFilePath(file, filepathLists);
            } else {
                filepath = file.toString();
                filepathLists.add(filepath);
            }
        }
        return filepathLists;
    }

}
