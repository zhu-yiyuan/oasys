package com.yygc.web.controller.comm;

import com.yygc.common.response.ApiRestResponse;
import com.yygc.domain.vo.Employee;
import com.yygc.domain.vo.Task;
import com.yygc.domain.vo.TaskDetail;
import com.yygc.domain.vo.User;
import com.yygc.service.admin.UserManagementService;
import com.yygc.service.comm.MyTasksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/user/comm")
public class MyTaskController {
    @Autowired
    private MyTasksService myTasksService;
    @Autowired
    private UserManagementService userManagementService;

    @RequestMapping("/selectTasks")
    @ResponseBody
    public List<Task> myTasks(HttpSession session) {
        User user = (User) session.getAttribute("user");
        Employee employee = userManagementService.selectEmployee(user.getUserId());
        List<TaskDetail> myTasks = myTasksService.selectMyTasks(employee.getEmployeeId());
        List<Task> tasks = new ArrayList<>();
        for (TaskDetail detail : myTasks) {
            if (detail.getTask().getTaskState().equals("0")) {
                detail.getTask().setTaskState("未完成");
            } else {
                detail.getTask().setTaskState("已完成");
            }
            tasks.add(detail.getTask());
        }
        return tasks;
    }

    @PostMapping("/updateTasksState")
    @ResponseBody
    public ApiRestResponse<String> updateTasksState(@RequestBody List<Integer> taskIds) {
        ApiRestResponse<String> apiRestResponse = new ApiRestResponse<>();
        int rows = myTasksService.updateTasksState(taskIds);
        System.out.println(rows);
        if (rows > 0) {
            apiRestResponse.setMessage("success");
        } else {
            apiRestResponse.setMessage("failure");
        }
        return apiRestResponse;
    }
}
