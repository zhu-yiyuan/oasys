package com.yygc.web.controller.comm;

import com.yygc.common.exception.WebException;
import com.yygc.common.response.Result;
import com.yygc.web.pojo.Attendance;
import com.yygc.web.service.AttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class CommAttendanceController {
    @Autowired
    AttendanceService attendanceService;

    @Autowired
    RestTemplate restTemplate;

    /**
     * 添加考情记录信息--上班考勤
     *
     * @param request
     * @param parameter
     * @return
     */
    @PostMapping("/user/duty/on")
    public Result onDuty(HttpServletRequest request, @RequestBody Map<String, Object> parameter) {
        Integer userId = Integer.parseInt((String) parameter.get("userId"));
        String site = (String) parameter.get("site");
        Attendance attendance = new Attendance(userId, site, new Date());
        attendanceService.setAttendanceStatus(attendance);

        if (attendanceService.addOneAttendance(attendance) < 1) {
            throw new WebException("添加考勤记录失败");
        }

        return new Result(200, "添加考勤记录成功", null);
    }

    /**
     * 添加考勤记录信息--下班考勤
     *
     * @param request
     * @param parameter
     * @return
     */
    @PostMapping("/user/duty/off")
    public Result offDuty(HttpServletRequest request, @RequestBody Map<String, Object> parameter) {
        Integer userId = Integer.parseInt((String) parameter.get("userId"));
        String site = (String) parameter.get("site");
        Attendance attendance = new Attendance(userId, site, new Date());
        attendanceService.setAttendanceStatus(attendance);
        if (attendanceService.addOneAttendance(attendance) < 1) {
            throw new WebException("添加考勤记录失败");
        }

        return new Result(200, "添加考勤记录成功", null);
    }

    @GetMapping("/user/duty")
    public Result getResult(HttpServletRequest request, @RequestBody Map<String, Object> parameter) {
//        Integer userId = Integer.parseInt((String) parameter.get("userId"));
        Integer userId = new Integer(123);
        Attendance attendance = new Attendance(userId);

        List<Attendance> result = attendanceService.queryListAttendance(attendance);

        return new Result(200, null, result);
    }

    public Result getTableOfOneMonth(HttpServletRequest request, @RequestBody Map<String, Object> parameter) {

        return null;
    }

    /**
     * 获取公司地址，调用第三方接口，修改公司的经纬度信息
     *
     * @param request
     * @param parameter
     * @return
     */
    @PostMapping("/admin/company/address")
    public Result modifyComAdd(HttpServletRequest request, @RequestBody Map<String, Object> parameter) {
        String comAdd = (String) parameter.get("companyAddress");
        String ak = "A4thcgxsrQG4RbGm0A8RUsxeC8gv3WBg";
        String url = "https://api.map.baidu.com/geocoding/v3/?address=" + comAdd + "&output=json&ak=" + ak;

        String result = restTemplate.getForObject(url, String.class);
        attendanceService.getLatLngFromRes(result);
        attendanceService.setCompanyLocation(comAdd);

        return new Result(200, "成功修改公司地址", result);
    }

}
