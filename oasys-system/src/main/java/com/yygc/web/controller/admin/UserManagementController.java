package com.yygc.web.controller.admin;

import com.yygc.domain.vo.User;
import com.yygc.common.response.ApiRestResponse;
import com.yygc.common.response.Constant;
import com.yygc.service.admin.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
public class UserManagementController {
    @Autowired
    UserManagementService userManagementService;

    @RequestMapping("/admin/selectAllEmployee")
    public List<User> selectAllEmployee(HttpSession session) {
        User user = (User) session.getAttribute("user");
        return userManagementService.selectAllEmployee(user.getUserId());
    }

    @RequestMapping("/admin/deleteEmployee")
    public ApiRestResponse<String> deleteEmployee(@RequestBody List<Integer> employeeIds) {
        ApiRestResponse<String> apiRestResponse = new ApiRestResponse<>();
        int i = userManagementService.deleteEmployee(employeeIds);
        if (i > 0) {
            apiRestResponse.setStatus(Constant.STATUS_DELETE_SUCCESS);
            apiRestResponse.setMessage("删除成功");
            return apiRestResponse;
        } else {
            apiRestResponse.setStatus(Constant.STATUS_DELETE_ERROR);
            apiRestResponse.setMessage("删除失败");
            return apiRestResponse;
        }
    }

    @RequestMapping("/admin/createEmployee")
    public String createEmployee(String user_account, String dept_name, HttpServletRequest httpServletRequest) {
        httpServletRequest.getSession();
        return "success";
    }
}
