package com.yygc.web.controller.admin;

import com.yygc.common.response.ApiRestResponse;
import com.yygc.common.response.Constant;
import com.yygc.common.utils.FileUploadUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
public class FileUploadController {
    @RequestMapping("/admin/fileupload")
    public String fileUpload() {
        return "admin/fileupload";
    }

    @RequestMapping("/admin/fileupload/files")
    @ResponseBody
    public ApiRestResponse<String> file(@RequestBody MultipartFile[] multipartFile) {
        ApiRestResponse<String> apiRestResponse = new ApiRestResponse<>();
        String pathFileName = "";
        for (MultipartFile file : multipartFile) {
            try {
                pathFileName = FileUploadUtil.upload(file);
            } catch (IOException e) {
                System.out.println(e.getMessage());
                apiRestResponse.setStatus(Constant.STATUS_UPLOAD_ERROR);
                apiRestResponse.setMessage("上传失败");
                e.printStackTrace();
                return apiRestResponse;
            }
        }
        apiRestResponse.setStatus(Constant.STATUS_UPLOAD_SUCCESS);
        apiRestResponse.setMessage("上传成功");
        return apiRestResponse;
    }
}
