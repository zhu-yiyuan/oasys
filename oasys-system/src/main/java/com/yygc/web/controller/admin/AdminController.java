package com.yygc.web.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user/admin")
public class AdminController {
    @RequestMapping("/admin-index")
    public String toIndex() {
        return "admin/admin-index";
    }

    @RequestMapping("/index")
    public String index() {
        return "admin/index";
    }

    @RequestMapping("/user-management")
    public String userManagement() {
        return "admin/user-management";
    }

    @RequestMapping("/task")
    public String task() {
        return "admin/mail";
    }

    @RequestMapping("/mail")
    public String mail() {
        return "admin/mail";
    }

    @RequestMapping("/attendance")
    public String attendance() {
        return "admin/attendance";
    }

    @RequestMapping("/process")
    public String process() {
        return "admin/process";
    }
}
