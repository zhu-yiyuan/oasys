package com.yygc.web.controller.admin;

import com.yygc.common.response.Constant;
import com.yygc.domain.vo.Task;
import com.yygc.domain.vo.User;
import com.yygc.common.response.ApiRestResponse;
import com.yygc.service.admin.TasksDetailService;
import com.yygc.service.admin.TasksService;
import com.yygc.service.admin.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class TaskController {
    @Autowired
    TasksService tasksService;
    @Autowired
    UserManagementService userManagementService;
    @Autowired
    TasksDetailService tasksDetailService;

    @RequestMapping("/admin/tasks")
    public String tasks() {
        return "admin/task";
    }

    @RequestMapping("/admin/publicTasks")
    @ResponseBody
    public ApiRestResponse<Task> publishTasks(@RequestBody Task task, HttpSession session) {
        User user = (User) session.getAttribute("user");
        int userId = user.getUserId();
        task.setCreateId(userId);
        List<Integer> employeeIds = userManagementService.findAllEmployee(userId);
        int i = tasksService.createTasks(task);
        int j = tasksDetailService.createTasksDetail(userId, employeeIds);
        ApiRestResponse<Task> apiRestResponse = new ApiRestResponse<>();
        if (i > 0 && j > 0) {
            apiRestResponse.setStatus(Constant.STATUS_PUBLIC_TASKS_SUCCESS);
            apiRestResponse.setMessage("录入成功");
            return apiRestResponse;
        }
        apiRestResponse.setStatus(Constant.STATUS_LOGIN_ERROR);
        apiRestResponse.setMessage("录入失败");
        return apiRestResponse;
    }

    @RequestMapping("/admin/selectAllTasks")
    @ResponseBody
    public List<Task> selectAllTasks() {
        return tasksService.selectAllTasks();
    }

    @PostMapping("/admin/deleteTasks")
    @ResponseBody
    public ApiRestResponse<String> deleteTasks(@RequestBody List<Integer> taskIds) {
        ApiRestResponse<String> apiRestResponse = new ApiRestResponse<>();
        int rows = tasksService.delectTasks(taskIds);
        if (rows > 0) {
            apiRestResponse.setStatus(Constant.STATUS_DELETE_SUCCESS);
            apiRestResponse.setMessage("删除成功");
            return apiRestResponse;
        }
        apiRestResponse.setStatus(Constant.STATUS_DELETE_ERROR);
        apiRestResponse.setMessage("删除失败");
        return apiRestResponse;
    }
}
