package com.yygc.web.controller.comm;

import com.yygc.common.response.Result;
import com.yygc.web.pojo.Process;
import com.yygc.web.pojo.*;
import com.yygc.web.service.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class ProcessController {
    @Resource
    EmployeeService employeeService;

    @Resource
    ReimburseService reimburseService;

    @Resource
    SubjectService subjectService;

    @Resource
    ExpenseService expenseService;

    @Resource
    ProcessService processService;

    @Resource
    OverTypeService overTypeService;

    @Resource
    OvertimeService overtimeService;

    @Resource
    FormalStaffService formalStaffService;

    @Resource
    LeaveTypeService leaveTypeService;

    @Resource
    LeaveService leaveService;

    @Resource
    DimissionService dimissionService;

    /**
     * 录入费用报销流程
     *
     * @param request
     * @param parameter
     * @return
     */
    @PostMapping("/user/process/expense")
    public Result applyExpense(HttpServletRequest request, @RequestBody Map<String, String> parameter) {
        String title = parameter.get("title");

        String proposer = parameter.get("proposer");
        Integer pro_id = employeeService.findEmpByName(proposer).getEmp_id();

        String reiWay = parameter.get("reiWay");
        Integer reiWay_id = reimburseService.findOneByName(reiWay).getRei_id();

        String verifier = parameter.get("verifier");
        Integer ver_id = employeeService.findEmpByName(verifier).getEmp_id();

        String reason = parameter.get("reason");

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = null;
        try {
            date = dateFormat.parse(parameter.get("date"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String subject = parameter.get("subject");
        Integer sub_id = subjectService.findOneByName(subject).getSub_id();

        Double count = Double.valueOf(parameter.get("count"));

        //缺少校验审核

        Expense one = new Expense(title, pro_id, reiWay_id, ver_id, reason, date, sub_id, count);
        expenseService.addOneExpense(one);

        Process oneProcess = new Process("费用报销", title, proposer, verifier, reason, new Date());
        processService.addOneProcess(oneProcess);

        return new Result(200, "费用报销流程申请成功", null);
    }

    /**
     * 录入加班补贴申请
     *
     * @param request
     * @param parameter
     * @return
     */
    @PostMapping("/user/process/overtime")
    public Result applyOverTime(HttpServletRequest request, @RequestBody Map<String, String> parameter) {
        //todo 获取表单信息
        String title = parameter.get("title");
        String proposer = parameter.get("proposer");
        String ovTypeName = parameter.get("typeName");
        String beginTime = parameter.get("beginTime");
        String endTime = parameter.get("endTime");
        String reason = parameter.get("reason");
        String verifier = parameter.get("verifier");

        Integer ovtyId = overTypeService.findOneByName(ovTypeName).getOvty_id();
        Integer veriId = employeeService.findEmpByName(verifier).getEmp_id();

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date beginDate = null;
        Date endDate = null;
        try {
            beginDate = dateFormat.parse(beginTime);
            endDate = dateFormat.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //todo 存入数据库表（process、overtime）
        Overtime overtime = new Overtime(title, ovtyId, beginDate, endDate, reason, veriId);
        overtimeService.addOne(overtime);

        Process process = new Process("加班补贴", title, proposer, verifier, reason, new Date());
        processService.addOneProcess(process);

        return new Result(200, "加班补贴流程申请成功！", null);
    }

    /**
     * 录入转正申请
     *
     * @param request
     * @param parameter
     * @return
     */
    @PostMapping("/user/process/staff")
    public Result applyFormalStaff(HttpServletRequest request, @RequestBody Map<String, String> parameter) {

        String title = parameter.get("title");
        String proposer = parameter.get("proposer");
        String verifier = parameter.get("verifier");
        String experience = parameter.get("experience");
        String understand = parameter.get("understand");
        String suggest = parameter.get("suggest");

        Integer pro_id = employeeService.findEmpByName(proposer).getEmp_id();
        Integer ver_id = employeeService.findEmpByName(verifier).getEmp_id();

        FormalStaff formalStaff = new FormalStaff(title, pro_id, ver_id, experience, understand, suggest);
        formalStaffService.addOneFormalStaff(formalStaff);

        Process process = new Process("转正申请", title, proposer, verifier, experience, new Date());
        processService.addOneProcess(process);

        return new Result(200, "转正流程申请成功！", null);
    }


    /**
     * 录入请假申请流程
     *
     * @param request
     * @param parameter
     * @return
     */
    @PostMapping("/user/process/holiday")
    public Result applyLeave(HttpServletRequest request, @RequestBody Map<String, String> parameter) {
        String title = parameter.get("title");
        String proposer = parameter.get("proposer");
        String typeName = parameter.get("typeName");
        String beginTime = parameter.get("beginTime");
        String endTime = parameter.get("endTime");
        String day = parameter.get("day");
        String verifier = parameter.get("verifier");
        String reason = parameter.get("reason");

        Integer prop_id = employeeService.findEmpByName(proposer).getEmp_id();
        Integer veri_id = employeeService.findEmpByName(verifier).getEmp_id();
        Integer leaTyp_id = leaveTypeService.findOneByName(typeName).getLea_id();
        Integer days = Integer.getInteger(day);

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date beginDate = null;
        Date endDate = null;

        try {
            beginDate = dateFormat.parse(beginTime);
            endDate = dateFormat.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Leave leave = new Leave(title, prop_id, leaTyp_id, beginDate, endDate, days, veri_id, reason);
        leaveService.addOneLeave(leave);

        Process process = new Process("请假申请", title, proposer, verifier, reason, new Date());
        processService.addOneProcess(process);

        return new Result(200, "请假流程申请成功！", null);
    }


    /**
     * 录入离职流程
     *
     * @param request
     * @param parameter
     * @return
     */
    @PostMapping("/user/process/dimission")
    public Result applyDimission(HttpServletRequest request, @RequestBody Map<String, String> parameter) {

        String title = parameter.get("title");
        String proposer = parameter.get("proposer");
        String heir = parameter.get("heir");
        String thing = parameter.get("thing");
        String reason = parameter.get("reason");
        String verifier = parameter.get("verifier");

        Integer dim_pro_id = employeeService.findEmpByName(proposer).getEmp_id();
        Integer dim_hand_id = employeeService.findEmpByName(heir).getEmp_id();
        Integer dim_ver_id = employeeService.findEmpByName(verifier).getEmp_id();

        Dimission dimission = new Dimission(title, dim_pro_id, dim_hand_id, thing, reason, dim_ver_id);
        dimissionService.addOneDimission(dimission);

        Process process = new Process("离职申请", title, proposer, verifier, reason, new Date());
        processService.addOneProcess(process);

        return new Result(200, "离职流程申请成功！", null);
    }

    @GetMapping("/admin/process")
    public List<Process> getProcess() {
        List<Process> result = processService.queryAllProcess();

        return result;
    }

    @DeleteMapping("/admin/process")
    public Result deleteProcess(@RequestBody Map<String, Object> parameter) {
        List<Integer> ids = (List<Integer>) parameter.get("ids");

        processService.deleteProcesses(ids);

        return new Result(200, "删除成功", null);
    }
}
