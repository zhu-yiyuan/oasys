package com.yygc.web.controller.admin;

import com.yygc.common.response.Result;
import com.yygc.web.pojo.Attendance;
import com.yygc.web.service.AttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class AdminAttendanceController {
    @Autowired
    AttendanceService attendanceService;

    @GetMapping("/admin/all/attendance")
    public List<Attendance> getAllAttendance() {
        List<Attendance> all = attendanceService.queryListAllAttendance();
        return all;
    }

    @DeleteMapping("/admin/attendances")
    public Result deleteAttendances(@RequestBody Map<String, Object> parameter) {
        List<Integer> ids = (List<Integer>) parameter.get("ids");

        attendanceService.removeAttendances(ids);

        return new Result(200, "删除成功", null);
    }
}
