package com.yygc.web.controller.admin;

import com.yygc.common.response.ApiRestResponse;
import com.yygc.common.response.Constant;
import com.yygc.common.utils.FileUploadUtil;
import com.yygc.domain.vo.Mail;
import com.yygc.service.admin.SendEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
public class MailController {
    @Autowired
    private SendEmailService sendEmailService;

    /**
     * 发送邮件
     *
     * @param mail 邮件实体类
     */
    @PostMapping("/admin/sendMail")
    @ResponseBody
    public ApiRestResponse<String> sendMail(Mail mail) {
//        if (!multipartFiles.isEmpty() && multipartFiles.size() > 0) {
//            for (MultipartFile file:multipartFiles) {
//                try{
//                    FileUploadUtil.upload(file);
//                } catch (IOException e) {
//                    System.out.println(e.getMessage());
//                    e.printStackTrace();
//                }
//            }
//        }
        ApiRestResponse<String> apiRestResponse = new ApiRestResponse<>();
        /**
         * 单附件上传
         */
        String pathFileName = "";
        try {
            pathFileName = FileUploadUtil.upload(mail.getMultipartFile());
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        sendEmailService.sendComplexEmail(mail.getTo(), mail.getSubject(), mail.getText(), FileUploadUtil.getDefaultBaseDir() + pathFileName);
        apiRestResponse.setStatus(Constant.STATUS_SEND_SUCCESS);
        apiRestResponse.setMessage("发送成功");
        return apiRestResponse;
    }
}
