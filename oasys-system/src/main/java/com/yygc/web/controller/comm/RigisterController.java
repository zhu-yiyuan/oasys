package com.yygc.web.controller.comm;

import com.yygc.domain.vo.Register;
import com.yygc.common.response.ApiRestResponse;
import com.yygc.common.response.Constant;
import com.yygc.service.comm.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RigisterController {
    @Autowired
    RegisterService registerService;

    @GetMapping("/user/comm/register")
    public String register() {
        return "comm/register";
    }

    @PostMapping("/user/comm/register")
    @ResponseBody
    public ApiRestResponse<Register> register(Register register){
        int userRow = registerService.createUser(register);
        int employeeRow = registerService.createEmployee(register);
        ApiRestResponse<Register> apiRestResponse = new ApiRestResponse<>();
        if (userRow > 0 && employeeRow > 0){
            apiRestResponse.setStatus(Constant.STATUS_REGISTER_SUCCESS);
            apiRestResponse.setMessage("注册成功");
            apiRestResponse.setData(register);
            return apiRestResponse;
        }
        apiRestResponse.setStatus(Constant.STATUS_REGISTER_ERROR);
        apiRestResponse.setMessage("注册失败");
        return apiRestResponse;
    }
}
