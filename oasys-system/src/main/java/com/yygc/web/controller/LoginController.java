package com.yygc.web.controller;

import com.yygc.common.utils.JWTUtil;
import com.yygc.common.utils.Md5Util;
import com.yygc.common.utils.RSAUtil;
import com.yygc.domain.vo.User;
import com.yygc.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.security.PrivateKey;

@Controller
public class LoginController {
    @Autowired
    private LoginService loginService;

    @Value("${privateKeyPath}")
    private String privateKeyPath;

    @GetMapping(value = {"/", "/login"})
    public String login() {
        return "login";
    }

    @PostMapping("/login")
    public ModelAndView login(User user, ModelAndView mav, HttpServletResponse response, HttpSession session) throws Exception {
        User u = loginService.selectUser(user.getUserAccount());
//        ApiRestResponse<User> apiRestResponse = new ApiRestResponse<>();
        String pwd = Md5Util.md5Digest(user.getUserPassword(), 156);
        System.out.println(pwd);
        if (u != null && pwd.equals(u.getUserPassword())) {
            int role = u.getRole();
            PrivateKey privateKey = RSAUtil.getPrivateKey(privateKeyPath);
            String token = JWTUtil.generateToken("abc", privateKey, 30);
            Cookie cookie = new Cookie("token", token);
            response.addCookie(cookie);
            session.setAttribute("user", u);
//            apiRestResponse.setStatus(Constant.STATUS_LOGIN_SUCCESS);
//            apiRestResponse.setData(u);
            if (role == 1) {
                mav.setViewName("redirect:user/comm/comm-index");
            } else {
                mav.setViewName("redirect:user/admin/admin-index");
            }
            return mav;
        }
//        apiRestResponse.setStatus(Constant.STATUS_LOGIN_ERROR);
//        apiRestResponse.setMessage("账号或密码错误");
        mav.addObject("message", "用户名或密码错误");
        mav.setViewName("login");
        return mav;
    }

    @RequestMapping("/logout")
    public ModelAndView logout(ModelAndView modelAndView, HttpServletResponse response) {
        Cookie cookie = new Cookie("token", null);
        cookie.setMaxAge(0);
        cookie.setSecure(true);
        cookie.setHttpOnly(true);
        cookie.setPath("/");
        response.addCookie(cookie);
        modelAndView.setViewName("redirect:/login");
        return modelAndView;
    }
}