package com.yygc.web.mapper;

import com.yygc.web.pojo.Attendance;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AttendanceMapper {
    int addOneAttendance(Attendance attendance);
    Attendance queryAttendance(Attendance attendance);
    List<Attendance> queryLastAttenToday(Attendance attendance);
    int deleteOneAttendance(Attendance attendance);
    List<Attendance> queryAll();
    int deleteAttendances(List<Integer> ids);
}
