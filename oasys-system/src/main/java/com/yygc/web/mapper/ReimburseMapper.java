package com.yygc.web.mapper;

import com.yygc.web.pojo.Reimburse;
import org.springframework.stereotype.Repository;

@Repository
public interface ReimburseMapper {
    public Reimburse findOneByTypeName(String typeName);
}
