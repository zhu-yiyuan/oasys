package com.yygc.web.mapper;

import com.yygc.web.pojo.Dimission;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DimissionMapper {
    int addOneDimission(Dimission dimission);
}
