package com.yygc.web.mapper;

import com.yygc.web.pojo.EmployeeHGC;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EmployeeMapper {
    EmployeeHGC findEmpByName(String name);
}
