package com.yygc.web.mapper;

import com.yygc.web.pojo.Subject;
import org.springframework.stereotype.Repository;

@Repository
public interface SubjectMapper {
    Subject findOneByName(String name);
}
