package com.yygc.web.mapper;

import com.yygc.web.pojo.Overtime;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OvertimeMapper {
    int addOne(Overtime overtime);
}
