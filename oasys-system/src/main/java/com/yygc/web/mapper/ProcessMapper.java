package com.yygc.web.mapper;

import com.yygc.web.pojo.Process;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ProcessMapper {
    int addOne(Process process);
    List<Process> queryListByProposer(String proposer);
    List<Process> queryListByVerifier(String verifier);
    int updateProcessCheckedStatus(int pro_id, int status);
    List<Process> queryAllProcess();

    int deleteProcess(List<Integer> ids);
}
