package com.yygc.web.mapper;

import com.yygc.web.pojo.Leave;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LeaveMapper {
    int addOneLeave(Leave leave);
}
