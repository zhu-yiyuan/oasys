package com.yygc.web.mapper;

import com.yygc.web.pojo.OverType;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OverTypeMapper {
    OverType findOneById(int ovty_id);
    OverType findOneByName(String typeName);
}
