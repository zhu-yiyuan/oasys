package com.yygc.web.mapper;

import com.yygc.web.pojo.LeaveType;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LeaveTypeMapper {
    LeaveType findLeaveTypeByName(String typeName);
}
