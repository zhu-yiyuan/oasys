package com.yygc.web.mapper;

import com.yygc.web.pojo.Expense;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ExpenseMapper {
    int addOne(Expense expense);
}
