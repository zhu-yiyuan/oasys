package com.yygc.web.mapper;

import com.yygc.web.pojo.FormalStaff;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FormalStaffMapper {
    public int addOne(FormalStaff formalStaff);
}
