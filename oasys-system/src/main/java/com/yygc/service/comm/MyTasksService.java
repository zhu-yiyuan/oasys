package com.yygc.service.comm;

import com.yygc.domain.vo.Task;
import com.yygc.domain.vo.TaskDetail;

import java.util.List;

public interface MyTasksService {
    public List<TaskDetail> selectMyTasks(Integer employee_id);

    public int updateTasksState(List<Integer> taskIds);
}
