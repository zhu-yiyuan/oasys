package com.yygc.service.comm;

import com.yygc.domain.vo.Register;

public interface RegisterService {
    public int createUser(Register register);
    public int createEmployee(Register register);
}
