package com.yygc.service.admin;

import com.yygc.domain.vo.TaskDetail;

import java.util.List;

public interface TasksDetailService {
    public int createTasksDetail(Integer taskId, List<Integer> employeeId);
    public List<TaskDetail> selectTaskDetail();
}
