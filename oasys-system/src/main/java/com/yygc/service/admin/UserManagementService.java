package com.yygc.service.admin;

import com.yygc.domain.vo.Employee;
import com.yygc.domain.vo.User;

import java.util.List;

public interface UserManagementService {
    public List<User> selectAllEmployee(Integer mgr);

    public int deleteEmployee(List<Integer> userAccounts);

    public List<Integer> findAllEmployee(Integer mgr);

    public Employee selectEmployee(Integer userId);
}
