package com.yygc.service.admin;

import com.yygc.domain.vo.Task;

import java.util.List;

public interface TasksService {
    public int createTasks(Task task);
    public List<Task> selectAllTasks();
    public int delectTasks(List<Integer> taskIds);
}
