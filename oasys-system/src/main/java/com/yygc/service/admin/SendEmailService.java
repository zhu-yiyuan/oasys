package com.yygc.service.admin;

public interface SendEmailService {
    public void sendComplexEmail(String to,String subject,String text,String filePath);
}
