package com.yygc.service;

import com.yygc.domain.vo.User;

public interface LoginService {
    public User selectUser(String userAccount);
}
