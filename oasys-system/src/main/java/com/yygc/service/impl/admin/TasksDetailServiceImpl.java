package com.yygc.service.impl.admin;

import com.yygc.domain.vo.TaskDetail;
import com.yygc.mapper.admin.TasksDetailMapper;
import com.yygc.service.admin.TasksDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TasksDetailServiceImpl implements TasksDetailService {
    @Autowired
    TasksDetailMapper tasksDetailMapper;

    @Override
    public int createTasksDetail(Integer taskId, List<Integer> employeeId) {
        return tasksDetailMapper.createTasksDetail(taskId, employeeId);
    }

    public List<TaskDetail> selectTaskDetail() {
        return tasksDetailMapper.selectTaskDetail();
    }
}
