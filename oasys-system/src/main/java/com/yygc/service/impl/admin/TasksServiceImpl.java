package com.yygc.service.impl.admin;

import com.yygc.domain.vo.Task;
import com.yygc.mapper.admin.TasksMapper;
import com.yygc.service.admin.TasksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TasksServiceImpl implements TasksService {
    @Autowired
    TasksMapper tasksMapper;

    @Override
    public int createTasks(Task task) {
        return tasksMapper.createTasks(task);
    }

    @Override
    public List<Task> selectAllTasks() {
        return tasksMapper.selectAllTasks();
    }

    @Override
    public int delectTasks(List<Integer> taskIds) {
        return tasksMapper.delectTasks(taskIds);
    }
}
