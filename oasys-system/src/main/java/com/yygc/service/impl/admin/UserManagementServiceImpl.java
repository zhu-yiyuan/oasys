package com.yygc.service.impl.admin;

import com.yygc.domain.vo.Employee;
import com.yygc.domain.vo.User;
import com.yygc.mapper.admin.UserManagementMapper;
import com.yygc.service.admin.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserManagementServiceImpl implements UserManagementService {
    @Autowired
    UserManagementMapper userManagementMapper;

    @Override
    public List<User> selectAllEmployee(Integer mgr) {
        return userManagementMapper.selectAllEmployee(mgr);
    }

    @Override
    public int deleteEmployee(List<Integer> employeeIds) {
        return userManagementMapper.deleteEmployee(employeeIds);
    }

    @Override
    public List<Integer> findAllEmployee(Integer mgr) {
        return userManagementMapper.findAllEmployee(mgr);
    }

    @Override
    public Employee selectEmployee(Integer employeeId) {
        return userManagementMapper.selectEmployee(employeeId);
    }
}
