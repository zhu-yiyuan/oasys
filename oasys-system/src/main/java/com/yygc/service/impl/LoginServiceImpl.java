package com.yygc.service.impl;

import com.yygc.domain.vo.User;
import com.yygc.mapper.LoginMapper;
import com.yygc.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {
    @Autowired
    LoginMapper loginMapper;

    @Override
    public User selectUser(String userAccount) {
        return loginMapper.selectUser(userAccount);
    }
}
