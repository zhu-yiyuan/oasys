package com.yygc.service.impl.comm;

import com.yygc.common.utils.Md5Util;
import com.yygc.domain.vo.Register;
import com.yygc.mapper.comm.RegisterMapper;
import com.yygc.service.comm.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegisterServiceImpl implements RegisterService {
    @Autowired
    RegisterMapper registerMapper;

    @Override
    public int createUser(Register register) {
        Integer salt = 156;
        String pwd = Md5Util.md5Digest(register.getUserPassword(), salt);
        register.setSalt(salt);
        register.setUserPassword(pwd);
        return registerMapper.createUser(register);
    }

    @Override
    public int createEmployee(Register register) {
        return registerMapper.createEmployee(register);
    }
}
