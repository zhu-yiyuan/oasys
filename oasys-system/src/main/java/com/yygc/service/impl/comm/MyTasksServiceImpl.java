package com.yygc.service.impl.comm;

import com.yygc.domain.vo.TaskDetail;
import com.yygc.mapper.comm.MyTasksMapper;
import com.yygc.service.comm.MyTasksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyTasksServiceImpl implements MyTasksService {
    @Autowired
    private MyTasksMapper myTasksMapper;

    @Override
    public List<TaskDetail> selectMyTasks(Integer employee_id) {
        return myTasksMapper.selectMyTasks(employee_id);
    }

    @Override
    public int updateTasksState(List<Integer> taskIds) {
        return myTasksMapper.updateTasksState(taskIds);
    }
}
