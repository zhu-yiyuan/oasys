package com.yygc.mapper.comm;

import com.yygc.domain.vo.Register;

public interface RegisterMapper {
    public int createUser(Register register);
    public int createEmployee(Register register);
}
