package com.yygc.mapper.comm;

import com.yygc.domain.vo.TaskDetail;

import java.util.List;

public interface MyTasksMapper {
    public List<TaskDetail> selectMyTasks(Integer employee_id);

    public int updateTasksState(List<Integer> taskIds);
}
