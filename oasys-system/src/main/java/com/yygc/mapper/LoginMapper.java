package com.yygc.mapper;

import com.yygc.domain.vo.User;

public interface LoginMapper {
    public User selectUser(String userAccount);
}
