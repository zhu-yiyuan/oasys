package com.yygc.mapper.admin;

import com.yygc.domain.vo.TaskDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TasksDetailMapper {
    public int createTasksDetail(@Param("taskId") Integer taskId, @Param("employeeIds") List<Integer> employeeIds);

    public List<TaskDetail> selectTaskDetail();
}
