package com.yygc.mapper.admin;

import com.yygc.domain.vo.Task;

import java.util.List;

public interface TasksMapper {
    public int createTasks(Task task);

    public List<Task> selectAllTasks();

    public int delectTasks(List<Integer> taskIds);
}
