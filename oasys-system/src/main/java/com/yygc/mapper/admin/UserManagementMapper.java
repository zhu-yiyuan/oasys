package com.yygc.mapper.admin;

import com.yygc.domain.vo.Employee;
import com.yygc.domain.vo.User;

import java.util.List;

public interface UserManagementMapper {
    public List<User> selectAllEmployee(Integer mgr);

    public int deleteEmployee(List<Integer> userAccounts);

    public List<Integer> findAllEmployee(Integer mgr);

    //获取部门
    public int createEmployee(String user_account, String dept_name);

    public Employee selectEmployee(Integer userId);
}
