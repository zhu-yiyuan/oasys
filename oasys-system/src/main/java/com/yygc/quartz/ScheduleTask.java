package com.yygc.quartz;

import com.yygc.domain.vo.TaskDetail;
import com.yygc.mapper.admin.TasksDetailMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 定时任务
 */
//@Component
public class ScheduleTask {
    @Autowired
    TasksDetailMapper tasksDetailMapper;

    //待办
    @Scheduled(cron = "0 * * * * MON-FRI")
    public void check() {
        List<TaskDetail> tasksDetailList = tasksDetailMapper.selectTaskDetail();
        if (tasksDetailList != null) {
            Date date = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String str = simpleDateFormat.format(date);
            for (TaskDetail taskDetail : tasksDetailList) {
                System.out.println(taskDetail.getEmployeeId() + "号员工的" + taskDetail.getTaskId() + "号通知公告还没有完成！" + " " + str);
            }
        } else {
            System.out.println("所有员工已完成通知公告！");
        }
    }
}
